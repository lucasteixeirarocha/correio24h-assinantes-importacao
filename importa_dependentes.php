<?php

set_time_limit(0);
error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('America/Bahia');
require_once("config/config.php");
define('SHOWLOG', true);
define('VERBOSE',true);


class DbQueryException  extends Exception{
  
}

class DbException  extends Exception{
  
}


class Database {
  var $mysqli;
  
  function connect($host,$user,$password,$database) {
    $this->mysqli = @mysqli_connect($host, $user, $password, $database);

    if (!$this->mysqli) {
        throw new DbException ("Falha ao conectar a {$host}. Erro: " . mysqli_connect_error());                
    }
    
    return $this->mysqli;
  }
  
  function truncate($table) {
    $ret = $this->mysqli->query("truncate table {$table}");
    if ($ret !== true) {
      throw new DbException ("Falha ao executar truncate na tabela {$table}.");                
    }    
  }
  
  function renameTable($old,$new) {
    $ret = $this->mysqli->query("RENAME TABLE $old TO $new");
    if ($ret !== true) {
      throw new DbException ("Falha ao renomear {$old} para {$new}.");                
    }    
  }  
  
  function dropTable($table) {
    $ret = $this->mysqli->query("DROP TABLE {$table}");
    if ($ret !== true) {
      throw new DbException ("Falha ao remover {$table}.");                
    }    
  }   
  
  function createTmpTableFromTable($table){
    $now=time();
    $newTable="{$table}_{$now}";
    
    $ret = $this->mysqli->query("CREATE TABLE {$newTable} LIKE {$table}");
    if ($ret !== true) {
      throw new DbException ("Falha ao criar {$newTable}. Erro: ".mysqli_error($this->mysqli));                
    }        
    return $newTable;
  }
  
  function query($query) {
    $ret = $this->mysqli->query($query);
    if ($ret !== true) {
      throw new DbQueryException ("Falha ao executar query. Erro: ".mysqli_error($this->mysqli));                
    }    
  }

  function startTransaction() {
    $ret = $this->mysqli->query("start transaction");
    if ($ret !== true) {
      throw new Exception ("Falha ao iniciar transação.");                
    }
  }
  
  function rollbackTransaction() {
    $ret = $this->mysqli->query("rollback");
    if ($ret !== true) {
      throw new DbException ("Falha ao executar rollback na transação.");                
    }    
  }  
  
  function commitTransaction() {
    $ret = $this->mysqli->query("commit");
    if ($ret !== true) {
      throw new DbException ("Falha ao executar commit na transação.");                
    }    
    
  }  
  

  
}


class DependentsMapper {
  
  var $pid;
  var $db;
  
  function setPid($pid) {
    $this->pid=$pid;    
  }
  
  function setDb(&$db){
    $this->db=$db;  
  }
  
  function saveFileOnDatabase(&$fileObj,$tableName) {
    $dataFile = $fileObj->getFileArrayOfRows();
    
    $lineNumber=1;

    foreach ($dataFile as $row) {
      $row = trim($row);

      if (strlen($row) == 0) {
        continue;
      } else if ($lineNumber == 1) {
        $lineNumber++;
        continue;
      } else if ($fileObj->getLastLineNumber() == $lineNumber) {
        $lineNumber++;
        continue;        
      }

      list($dataFields,$totalFields) = $fileObj->getLineData($row);    
      
      list($dia,$mes,$ano)=explode("/",$dataFields[2]);
      $validade = mktime(0,0,0,$mes,$dia,$ano);
      $dataToSave['pid']=$this->pid;
      $dataToSave['nome'] = $dataFields[0];
      $dataToSave['deleted'] = 0;
      $dataToSave['hidden'] = 0;
      $dataToSave['nome_empresa'] = $dataFields[1];
      $dataToSave['validade'] = $validade;
      $dataToSave['id_numerico_rb'] = $dataFields[3];
        
      $this->newDependent($dataToSave,$tableName);  
      
      $lineNumber++;

    }    
  }
  
  function newDependent($data,$tableName){
    $now=time();
    
    $pid = $data['pid'];
    $tstamp = $now;
    $crdate = $now;
    $deleted = $data['deleted'];
    $hidden = $data['hidden'];
    $nome = trim($data['nome']);
    $nome_empresa = trim($data['nome_empresa']);
    $validade = trim($data['validade']);
    $id_numerico_rb = (int) trim($data['id_numerico_rb']);
    
    
    $INSERT="insert into {$tableName} (pid,
                                                   tstamp,
                                                   crdate,
                                                   deleted,
                                                   hidden,
                                                   nome,
                                                   nome_empresa,
                                                   validade,
                                                   id_numerico_rb)
                                            value  ({$pid},
                                                    {$tstamp},
                                                    {$crdate},
                                                    {$deleted},
                                                    {$hidden},
                                                    '{$nome}',
                                                    '{$nome_empresa}',
                                                    {$validade},
                                                    {$id_numerico_rb})";
    $this->db->query($INSERT);
  }
}


class Console {

    static public function showHelp() {
        $programname = basename(__FILE__);
        $text = <<<EOT
Uso: php {$programname} <ARQUIVO_A_IMPORTAR>


EOT;
        echo $text;
    }
    static function processParameters($argv) {
        // print_r($argv);
        $result = array();
        if (count($argv) == 2) {
            $filename = $argv[1];

            if (!file_exists($filename)) {
                echo "\033[0;31m\nArquivo a importar nao encontrado: $filename.\n\033[0m" . PHP_EOL;
                self::showHelp();
                return false;
            }
          
            $result['file'] = $filename;
          
        } else {
            self::showHelp();
            return false;
        }
        return $result;
    }

}




class ProcessFile {
  var $fileContent;
  var $fileByLine;
  var $informedTotalLines=0;
  var $errors;
  var $lastLineNumber;
  
  function _construct(){
    $this->firstLineValidate = false;
    $this->lastLineValidate  = false;
    $this->lineDataValidate  = array();
    $this->errors = array();
    $this->lastLineNumber = 0;
  }
  
  function readFile($file) {
    
    $this->fileContent=file_get_contents($file);    
    $this->fileContent = preg_replace('/^\h*\v+/m', '', $this->fileContent);
    $this->fileByLine=explode("\n",$this->fileContent);  
    
  }
  
  function getFileArrayOfRows(){
    return $this->fileByLine;
  }
  
  function getLastLineNumber(){
    return $this->lastLineNumber;
  }
  
  function validateFile(){
    $lineNumber=1;
    
    foreach( $this->fileByLine as $row) {

      $row = trim($row);
      
      if (strlen($row) == 0) {
        continue;
      } 
      
      list($dataFields,$totalFields) = $this->getLineData($row);
      
      if ($totalFields == 1 && $lineNumber == 1){
        
        $this->firstLineValidate = $this->validateFirstFile($row); 
        
      } else if ($totalFields == 1) {
        
        $this->lastLineValidate = $this->validateLastFile($row);
        
        if ($this->lastLineValidate) {
          $this->lastLineNumber=$lineNumber;
        }
        
      } else {
        
        $ret = $this->validateDataRow($dataFields);
        if ($ret !== true) {
          $this->errors[$lineNumber]=$ret;
        }        
        
      }
      
      $lineNumber++;
    } 
    
    $lineNumber = $lineNumber - 3; // Duas linhas de controle e uma soma no final do loop
        
    if ($this->informedTotalLines != $lineNumber) {
      $this->errors[$lineNumber][] = "Total de linhas informadas diferente das linhas lidas.";
    }

    if ($this->firstLineValidate 
          && $this->lastLineValidate 
          && count($this->errors) == 0) {
      
      return true;
      
    }
    
    return false;
    
  }
  
  
  function showErrors() {
    if (count($this->errors) > 0) {
      foreach($this->errors as $line => $error) {
        foreach($error as $idx => $err) {
          echo "[ERRO] - LINHA:{$line} - {$err}\n";          
        }
      }      
    }
  }
  
  function validateFirstFile($line){
    return true;
  }
  
  function validateLastFile($line){
    
    $key    = substr($line,0,1);
    $number = (int) substr($line,1);
    
    if ($key == 'R'){
      $this->informedTotalLines = $number;
      return true;
    }
    
    return false;
  }

  function validateDataRow($data){
    $error=array();
    
    if (count($data) != 4) {
      $error[]="Número de campos incorreto, valor esperado: 5 campos.";
    } 

    if (empty($data[0])) {
      $error[]="O campo nome é obrigatório.";
    } 

    if (empty($data[2])) {      
      $error[]="A data de expiração é obrigatória.";      
    } else {      
      list($dia,$mes,$ano)=explode("/",$data[2]);      
      if (!checkdate($mes,$dia,$ano)){
        $error[]="A data de expiração está no formato errado, data informada: {$data[2]}.";
      }      
    }

    if (empty($data[3])) {
      $error[]="Código do assinante titular obrigatório.";
    } else {
      $cod = (int) $data[3];
      if ($cod == 0) {
        $error[]="Código do assinante titular obrigatório e deve ser númerico.";  
      }
      
    }

    if (count($error) > 0){
      return $error;
    }
    
    return true;
  }
  

  function getLineData($line){    
    
    $lineParts=explode(";",$line);
    
    $totalParts = count($lineParts);

    return array($lineParts,$totalParts);
  }
  
}

/*
 
 INICIO DO SCRIPT
 
*/


$params = Console::processParameters($argv);
if (!is_array($params)) {
    exit(1);
}

$importfile = $params['file'];

$file = new ProcessFile();
$mysqli = new Database();
$mapper = new DependentsMapper();

$mapper->setPid(PID);
$mapper->setDb($mysqli);

try {
  $file->readFile($importfile);
  $ret = $file->validateFile();

  if (!$ret){
    $file->showErrors();
    exit(1);
  }
  
  $mysqli->connect(DBHOST,DBUSER,DBPASS,DBNAME);  

  $newTableName = $mysqli->createTmpTableFromTable("tx_cwpageflip_dependente");
  
  $mysqli->startTransaction(); 
  
  $mapper->saveFileOnDatabase($file,$newTableName);
  
  $mysqli->renameTable('tx_cwpageflip_dependente','tx_cwpageflip_dependente_old');
  
  $mysqli->renameTable($newTableName,'tx_cwpageflip_dependente');
  
  $mysqli->dropTable('tx_cwpageflip_dependente_old');
  
  $mysqli->commitTransaction();
  
  echo "IMPORTACAO OK\n";
  
} catch (DbQueryException $e) {  
  $mysqli->rollbackTransaction();
  echo $e->getMessage()."\n";    
} catch (DbException $e) {    
  echo $e->getMessage()."\n";
} catch (Exception $e) {    
  echo $e->getMessage()."\n";
}

