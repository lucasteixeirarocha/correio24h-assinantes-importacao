<?php

set_time_limit(0);
ini_set('memory_limit', '256M');
error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('America/Bahia');
require_once("config/config.php");
define('SHOWLOG', true);
define('VERBOSE',false);

class ValidatorErrorException  extends Exception{
  
}

class ValidatorWarningException  extends Exception{
  
}



function showMessage($message){
  if (VERBOSE) {
    echo $message,"\n";
  }  
}

/**
 * Estatisticas do processamento do arquivo
 */
class Statistics {

    /**
     * Armazena os dados das estatisticas
     * @var array
     */
    private $_stats = array();

    /**
     * Construtor
     */
    function __construct() {
        
    }

    /**
     * Define o valor de um item
     * @param string $key
     * @param mixed $value 
     */
    public function set($key, $value) {
        $this->_stats[$key] = $value;
    }

    /**
     * Adiciona no array um item
     * @param string $key
     * @param mixed $value 
     */
    public function add($key, $value) {
        if (!isset($this->_stats[$key])) {
            $this->_stats[$key] = array();
        }
      
        if (is_string($value)) {
          $value=utf8_encode($value);        
        } 
      
        $this->_stats[$key][] = $value;
    }

    /**
     * Incrementa o valor de um item
     * @param string $key
     * @param int $value 
     */
    public function inc($key, $value) {
        if (!isset($this->_stats[$key])) {
            $this->_stats[$key] = 0;
        }
        $this->_stats[$key] ++;
    }

    /**
     * Elimina um item do array
     * @param string $key
     * @param mixed $value 
     */
    public function del($key, $value) {
        if (isset($this->_stats[$key])) {
            foreach ($this->_stats[$key] as $key2 => $value2) {
                if ($value == $value2) {
                    unset($this->_stats[$key2]);
                    return;
                }
            }
        }
    }

    /**
     * Obter o valor a partir de uma chave
     * @param string $key
     */
    public function get($key) {
        return $this->_stats[$key];
    }

    /**
     * Dump de todas variaveis
     */
    public function toString() {
        return print_r($this->_stats, true);
    }

}

/**
 * Classe de acesso ao banco de dados
 * 
 */
class DBAccess {

    /**
     * Objeto PDO
     * @var PDO 
     */
    private $_dbh = null;

    /**
     * Nome do host
     * @var string 
     */
    private $_dbhost = "";

    /**
     * Nome do banco
     * @var string 
     */
    private $_dbname = "";

    /**
     * Nome do usuario
     * @var string 
     */
    private $_dbuser = "";

    /**
     * Senha do usuario
     * @var string 
     */
    private $_dbpassword = "";

    /**
     * Construtor
     * 
     * @param string $host Nome do host
     * @param string $dbname Nome do banco de dados
     * @param string $user Nome do usuario
     * @param string $password Nome da senha
     */
    function __construct($host, $dbname, $user, $password) {
        $this->_dbhost = $host;
        $this->_dbname = $dbname;
        $this->_dbuser = $user;
        $this->_dbpassword = $password;
    }

    /**
     * Conexao PDO
     */
    public function getConnection() {

        if (is_object($this->_dbh)) {
            return $this->_dbh;
        }

        $dsn = 'mysql:host=' . $this->_dbhost . ';dbname=' . $this->_dbname;
        try {
          
            $this->_dbh = new PDO($dsn, $this->_dbuser, $this->_dbpassword);
            $this->_dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );          
          
        } catch (PDOException $e) {
            
            print "Error: " . $e->getMessage()."\n";
            print "{$dsn}\n";
          
            die();
        }
      

        return $this->_dbh;
    }

    /**
     * Construtor
     * 
     * @param PDO $db Objeto de conexao PDO
     */
    public function setConnection($db) {
        $this->_dbh = $db;
    }

    /**
     * Executa a consulta e retorna o conjunto de resultados
     * 
     * @param string $sql 
     * @return PDOStatement 
     */
    public function query($sql) {
        $dbh = $this->getConnection();
        return $dbh->query($sql);
    }

    /**
     * Executa a consulta e retorna o numero de linhas afetadas
     * 
     * @param string $sql 
     * @return int | boolean
     */
    public function exec($sql) {
        $dbh = $this->getConnection();
        return $dbh->exec($sql);
    }

    /**
     * Prepara a consulta para a execucao
     * 
     * @param string $sql 
     * @return PDOStatement 
     */
    public function prepare($sql) {
        $dbh = $this->getConnection();
        return $dbh->prepare($sql);
    }

    /**
     * Valor do ultimo ID inserido de uma tabela com campo do tipo autoincrement
     * 
     * @param string $name Nome da tabela
     * @return int
     */
    public function lastInsertId($name = NULL) {
        $dbh = $this->getConnection();
        return $dbh->lastInsertId($name);
    }

}

/**
 * Classe de acesso as tabelas do banco de dados
 * 
 */
class DataMapper {

    /**
     * @var DBAccess
     */
    protected static $_db;

    /**
     * Definindo conexao com o banco de dados
     * 
     * @param DBAccess $db
     */
    public static function init($db) {
        self::$_db = $db;
    }

    /**
     * Valor do �ltimo ID inserido de uma tabela com campo do tipo autoincrement
     * 
     * @param string $name Nome da tabela
     * @return DBAccess
     */
    public static function getDbAdapter() {
        return self::$_db;
    }

}

/**
 * Mapeamento de acesso a tabela tx_cwpageflip_users
 * 
 */
class UsersMapper extends DataMapper {

    public static function getAllByOrigin($origin) {

        $origin = trim($origin);
        $sql = <<<EOT
SELECT
  uid,
  pid,
  tstamp,
  crdate,
  cruser_id,
  deleted,
  hidden,
  nome,
  login,
  password,
  cpf,
  data,
  id_numerico_rb,
  email,
  natureza_juridica,
  data_expiracao_cartao,
  nome_responsavel_exibicao_cartao,
  flag_clubecorreio
FROM tx_cwpageflip_users
WHERE 
  (id_numerico_rb IS NOT NULL AND id_numerico_rb != '') 
  AND origem='{$origin}'
ORDER BY uid
EOT;

        $db = self::getDbAdapter();
        return $db->query($sql);
    }

    /**
     * Lista de assinantes ativos por origem
     * 
     * @param string $origin
     * @return PDOStatement 
     */
    public function getActiveByOrigin($origin) {

        $origin = trim($origin);
        $sql = <<<EOT
SELECT 
  uid,
  pid,
  tstamp,
  crdate,
  cruser_id,
  deleted,
  hidden,
  nome,
  login,
  password,
  cpf,
  data,
  id_numerico_rb,
  email,
  natureza_juridica,
  data_expiracao_cartao,
  nome_responsavel_exibicao_cartao,
  flag_clubecorreio
FROM tx_cwpageflip_users
WHERE
  (id_numerico_rb IS NOT NULL AND id_numerico_rb != '') 
  AND hidden=0 
  AND deleted=0
  AND origem='{$origin}'
ORDER BY uid
EOT;
        $db = self::getDbAdapter();
        return $db->query($sql);
    }

    public static function getAllFormWeb() {

        $origin = trim($origin);
        $sql = <<<EOT
SELECT
  uid,
  pid,
  tstamp,
  crdate,
  cruser_id,
  deleted,
  hidden,
  nome,
  login,
  password,
  cpf,
  data,
  id_numerico_rb,
  email,
  natureza_juridica,
  data_expiracao_cartao,
  nome_responsavel_exibicao_cartao,
  flag_clubecorreio
  
FROM tx_cwpageflip_users
WHERE 
  origem='FormWeb'
ORDER BY uid
EOT;

        $db = self::getDbAdapter();
        return $db->query($sql);
    }

    /**
     * Quantidade de assinantes que ja efetivaram o primeiro login
     * 
     * @return int 
     */
    public function getListFirstLogin() {

        $sql = <<<EOT
SELECT COUNT(*) AS total
FROM tx_cwpageflip_users 
WHERE 
  ( id_numerico_rb IS NOT NULL AND id_numerico_rb != '' )
  AND ( email != '' AND email IS NOT NULL )
  AND ( primeiro_acesso IS NOT NULL AND primeiro_acesso != '' )
  AND deleted=0 AND hidden=0 
EOT;
        $db = self::getDbAdapter();
        $stmt = $db->query($sql);
        return $stmt->fetchColumn();
    }

    /**
     * Adiciona um novo assinante
     * 
     * @param string $pid      PageID
     * @param string $name     Nome do assinante
     * @param string $email    E-mail do assinante 
     * @param string $login    login do Assinante
     * @param string $password Senha do Assinante
     * @param string $cpf      CPF do assinante
     * @param string $serial   ID do sistema da assinatura
     * @param string $origin   Tipo de cadastro
     * 
     * @return int 
     */
    public function createNew($pid, $name, $email, $login, $password, $cpf, $serial, $origin,$natureza_juridica,$data_expiracao_cartao,$nome_responsavel_exibicao_cartao,$flag_clubecorreio) {
      
      showMessage("DEBUG createNew USER: {$cpf}\n");
      
        $params = array();
        $params[':pid'] = $pid;
        $params[':name'] = $name;
        $params[':email'] = $email;
        $params[':login'] = $login;
        $params[':password'] = $password;
        $params[':cpf'] = $cpf;
        $params[':serial'] = $serial;
        $params[':origin'] = $origin;
        $params[':natureza_juridica'] = $natureza_juridica;
        $params[':data_expiracao_cartao'] = $data_expiracao_cartao;
        $params[':nome_responsavel_exibicao_cartao'] = $nome_responsavel_exibicao_cartao;
        $params[':flag_clubecorreio'] = $flag_clubecorreio;

        $sql = <<<EOT
INSERT INTO tx_cwpageflip_users (
pid,
tstamp,
crdate,
cruser_id,
deleted,
hidden,
nome,
email,
login,
password,
cpf,
data,
id_numerico_rb,
origem,
natureza_juridica,
data_expiracao_cartao,
nome_responsavel_exibicao_cartao,
flag_clubecorreio
) VALUES (
:pid,
UNIX_TIMESTAMP(),
UNIX_TIMESTAMP(),
1,
0,
0,
:name,
:email,
:login,
:password,
:cpf,
NOW(),
:serial,
:origin,
:natureza_juridica,
:data_expiracao_cartao,
:nome_responsavel_exibicao_cartao,
:flag_clubecorreio
)
EOT;
        $db = self::getDbAdapter();
        $sth = $db->prepare($sql);      
      return $sth->execute($params);
      
    }

    /**
     * Atualiza o nome do e-mail
     * 
     * @param $email  E-mail do assinante
     * @param $cpf    CPF do assinate
     * @param $serial ID do sistema da assinatura
     * 
     * @return int
     */
    public function updateEmail($email, $cpf, $serial) {
      
        showMessage("DEBUG updateEmail USER: {$cpf}\n");
                    
        $params = array();
        $params[':email'] = $email;
        $params[':cpf'] = $cpf;
        $params[':serial'] = $serial;

        $sql = <<<EOT
UPDATE tx_cwpageflip_users SET 
  tstamp=UNIX_TIMESTAMP(),
  email=:email 
WHERE 
  cpf=:cpf 
  AND id_numerico_rb=:serial 
  AND email!=:email
EOT;
        $db = self::getDbAdapter();
        $sth = $db->prepare($sql);
        return $sth->execute($params);
    }

  
    /**
     * Atualiza o nome do e-mail
     * 
     * @param $email  E-mail do assinante
     * @param $cpf    CPF do assinate
     * @param $serial ID do sistema da assinatura
     * 
     * @return int
     */
    public function updateFlagClubeCorreio($flag_clubecorreio, $cpf, $serial) {
      
        showMessage("DEBUG flag_clubeCorreio USER: {$cpf}\n");
                    
        $params = array();
        $params[':flag_clubecorreio'] = $flag_clubecorreio;
        $params[':cpf'] = $cpf;
        $params[':serial'] = $serial;

        $sql = <<<EOT
UPDATE tx_cwpageflip_users SET 
  tstamp=UNIX_TIMESTAMP(),
  flag_clubecorreio=:flag_clubecorreio 
WHERE 
  cpf=:cpf 
  AND id_numerico_rb=:serial
EOT;
        $db = self::getDbAdapter();
        $sth = $db->prepare($sql);
        return $sth->execute($params);
    }  
  
    public function updateDataExpiracaoCartao($data_expiracao_cartao, $cpf, $serial) {

        showMessage("UPDATE updateDataExpiracaoCartao {$cpf} - ($data_expiracao_cartao)\n");
      
        $params = array();
        $params[':data_expiracao_cartao'] = $data_expiracao_cartao;
        $params[':cpf'] = $cpf;
        $params[':serial'] = $serial;

        $sql = <<<EOT
UPDATE tx_cwpageflip_users SET 
  tstamp=UNIX_TIMESTAMP(),
  data_expiracao_cartao=:data_expiracao_cartao 
WHERE 
  cpf=:cpf 
  AND id_numerico_rb=:serial
EOT;
        $db = self::getDbAdapter();
        $sth = $db->prepare($sql);
        return $sth->execute($params);
    }  
  
    public function updateNaturezaJuridica($natureza_juridica, $cpf, $serial) {
      
        showMessage("UPDATE updateNaturezaJuridica {$cpf}\n");
        $params = array();
        $params[':natureza_juridica'] = $natureza_juridica;
        $params[':cpf'] = $cpf;
        $params[':serial'] = $serial;

        $sql = <<<EOT
UPDATE tx_cwpageflip_users SET 
  tstamp=UNIX_TIMESTAMP(),
  natureza_juridica=:natureza_juridica 
WHERE 
  cpf=:cpf 
  AND id_numerico_rb=:serial
EOT;
        $db = self::getDbAdapter();
        $sth = $db->prepare($sql);
        return $sth->execute($params);
    }    

    public function updateNomeResponsavelExibicaoCartao($nome_responsavel_exibicao_cartao, $cpf, $serial) {
      
        showMessage("UPDATE updateNomeResponsavelExibicaoCartao {$cpf}\n");
        $params = array();
        $params[':nome_responsavel_exibicao_cartao'] = $nome_responsavel_exibicao_cartao;
        $params[':cpf'] = $cpf;
        $params[':serial'] = $serial;

        $sql = <<<EOT
UPDATE tx_cwpageflip_users SET 
  tstamp=UNIX_TIMESTAMP(),
  nome_responsavel_exibicao_cartao=:nome_responsavel_exibicao_cartao 
WHERE 
  cpf=:cpf 
  AND id_numerico_rb=:serial
EOT;
        $db = self::getDbAdapter();
        $sth = $db->prepare($sql);
        return $sth->execute($params);
    }    
  
  
    /**
     * Ativa uma assinatura
     * 
     * @param type $cpf    CPF da assinatura
     * @param type $serial ID do sistema da assinatura
     * 
     * @return int
     */
    public function activeUser($cpf, $serial) {

        showMessage("DEBUG activeUser USER: {$cpf}\n");
        $params = array();
        $params[':cpf'] = $cpf;
        $params[':serial'] = $serial;

        $sql = <<<EOT
UPDATE tx_cwpageflip_users SET 
  tstamp=UNIX_TIMESTAMP()
  ,hidden=0
  ,deleted=0 
WHERE 
  cpf=:cpf
  AND id_numerico_rb=:serial
EOT;
        $db = self::getDbAdapter();
        $sth = $db->prepare($sql);
        return $sth->execute($params);
    }

    /**
     * Desativa uma assinatura
     * 
     * @param type $uid    CPF da assinatura
     * @param type $origin ID do sistema da assinatura
     * 
     * @return int
     */
    public function deactiveUser($uid, $origin) {
        showMessage("DEBUG deactiveUser USER: {$cpf}\n");
        $params = array();
        $params[':uid'] = $uid;
        $params[':origin'] = $origin;

        $sql = <<<EOT
UPDATE tx_cwpageflip_users SET 
  hidden=1,
  deleted=1
WHERE 
  uid=:uid 
  AND origem=:origin
EOT;
        $db = self::getDbAdapter();
        $sth = $db->prepare($sql);
        return $sth->execute($params);
    }

    /**
     * Retorna a data de expiracao da assinatura
     * 
     * @return string
     */
    protected function _getExpirationDate() {

        $date = date("Y-m-d 23:59:59");
        return $date;
    }

    /**
     * Desativa todos os assinantes pela data de expiracao
     * 
     * @param string $origin Tipo de assinante / cadastro
     * @return int
     */
    public function deactiveExpired($origin) {
         
        showMessage("DEBUG deactiveExpired USER: {$cpf}\n");
                    
        $origin = trim($origin);
        $date = $this->_getExpirationDate();

        $sql = <<<EOT
UPDATE tx_cwpageflip_users
SET hidden=1,deleted=1
WHERE 
  datahora_expiracao_degustacao < UNIX_TIMESTAMP('{$date}')
  AND datahora_expiracao_degustacao > 0
  AND datahora_expiracao_degustacao IS NOT NULL
  AND origem='{$origin}'
EOT;
        $db = self::getDbAdapter();
        return $db->exec($sql);
    }

    /**
     * Lista todos os assinantes cuja a assinatura expirou por origem
     * 
     * @param string $origin
     * @return PDOStatement
     */
    public function getListExpired($origin) {

        $origin = trim($origin);
        $date = $this->_getExpirationDate();

        $sql = <<<EOT
SELECT
  uid,nome 
FROM tx_cwpageflip_users
WHERE 
  datahora_expiracao_degustacao < UNIX_TIMESTAMP('{$date}')
  AND datahora_expiracao_degustacao > 0
  AND datahora_expiracao_degustacao IS NOT NULL
  AND hidden=0 
  AND deleted=0
  AND origem='{$origin}'
EOT;
        $db = self::getDbAdapter();
        return $db->query($sql);
    }

}

/**
 *  Classe responsavel pelo registro do log de operacoes
 */
class LogMapper extends DataMapper {

    /**
     * Registra a mensagem de log
     * @param type $pid     PageID
     * @param type $userID  Id do assinante
     * @param type $message Mensagem a ser registrada
     * @return type 
     */
    public static function log($pid, $userID, $message) {
        $db = self::getDbAdapter();
        $message = trim($message);

        $pid = intval($pid);
        $userID = intval($userID);

        $sql = <<<EOT
INSERT INTO tx_cwpageflip_users_log(
pid,
tstamp,
crdate,
crdate_id,
deleted,
hidden,
user,
evento
) VALUES (
:pid,
UNIX_TIMESTAMP(),
UNIX_TIMESTAMP(),
NULL,
0,
0,
:userID,
:message
)
EOT;
        $sth = $db->prepare($sql);
        return $sth->execute(array(':pid' => $pid, ':userID' => $userID, ':message' => $message));
    }

}

/**
 * Assinantes
 */
class Subscribers {

    /**
     * Conexao de acesso ao banco de dados
     * @var DBAccess
     */
    protected static $_db = null;

    /**
     * Acesso a tabela de assinantes
     * @var UsersMapper
     */
    protected $_usersDb = null;

    /**
     * Construtor
     */
    function __construct(DBAccess $db) {
        self::$_db = $db;
    }

    /**
     * Define a conexao do banco de dados
     * @param DBAccess $db
     */
    public function setDbConnection(DBAccess $db) {
        self::$_db = $db;
    }

    /**
     * Obtem a conexao do banco de dados
     * @return DBAccess
     */
    public function getDbConnection() {
        return $this->_db;
    }

    /**
     * Obtem o objeto de acesso a tabela de assinantes
     * @return UsersMapper
     */
    public function getDbMapper() {
        if (!is_object($this->_usersDb)) {
            $this->_usersDb = new UsersMapper();
            $this->_usersDb->init(self::$_db);
        }
        return $this->_usersDb;
    }

    /**
     * Gera a senha inicial do assinante
     * @return string
     */
    protected function _generatePassword($value) {
        return substr(md5(uniqid(rand(), 1) . $value), 0, 7);
    }

    /**
     * Adiciona um novo assinante
     * 
     * @param string $pid      PageID
     * @param string $name     Nome do assinante
     * @param string $email    E-mail do assinante 
     * @param string $login    login do Assinante
     * @param string $password Senha do Assinante
     * @param string $cpf      CPF do assinante
     * @param string $serial   ID do sistema da assinatura
     * @param string $origin   Tipo de cadastro
     * 
     * @return int 
     */
    public function createNew($pid, $name, $email, $login, $password, $cpf, $serial, $origin,$natureza_juridica,$data_expiracao_cartao,$nome_responsavel_exibicao_cartao,$flag_clubecorreio) {
        $userdb = $this->getDbMapper();
        $hashpassword = $this->_generatePassword($password);
        return $userdb->createNew($pid, $name, $email, $login, $hashpassword, $cpf, $serial, $origin,$natureza_juridica,$data_expiracao_cartao,$nome_responsavel_exibicao_cartao,$flag_clubecorreio);
    }

    /**
     * Altera o e-mail do assinante
     * @param string $email  E-mail do assinante
     * @param string $cpf    CPF do assinante
     * @param string $serial Codigo do assinante
     * @return int
     */
    public function changeEmail($email, $cpf, $serial) {
        $userdb = $this->getDbMapper();
        return $userdb->updateEmail($email, $cpf, $serial);
    }
  
    /**
     * Altera o e-mail do assinante
     * @param string $email  E-mail do assinante
     * @param string $cpf    CPF do assinante
     * @param string $serial Codigo do assinante
     * @return int
     */
    public function changeFlagClubeCorreio($flag_clubecorreio, $cpf, $serial) {
        $userdb = $this->getDbMapper();
        return $userdb->updateFlagClubeCorreio($flag_clubecorreio, $cpf, $serial);
    }
  

    public function changeDataExpiracaoCartao($data_expiracao_cartao, $cpf, $serial) {
        $userdb = $this->getDbMapper();
        return $userdb->updateDataExpiracaoCartao($data_expiracao_cartao, $cpf, $serial);
    }

    public function changeNaturezaJuridica($natureza_juridica, $cpf, $serial) {
        $userdb = $this->getDbMapper();
        return $userdb->updateNaturezaJuridica($natureza_juridica, $cpf, $serial);
    }

    public function changeNomeResponsavelExibicaoCartao($nome_responsavel_exibicao_cartao, $cpf, $serial) {
        $userdb = $this->getDbMapper();
        return $userdb->updateNomeResponsavelExibicaoCartao($nome_responsavel_exibicao_cartao, $cpf, $serial);
    }
  
  
    /**
     * Ativa a assinatura
     * @param string $cpf 
     * @param string $serial
     * @return int
     */
    public function active($cpf, $serial) {
        $userdb = $this->getDbMapper();
        return $userdb->activeUser($cpf, $serial);
    }

    /**
     * Desativa a assinatura
     * @param string $uid
     * @param string $origin
     * @return int
     */
    public function deactive($uid, $origin) {
        $userdb = $this->getDbMapper();
        return $userdb->deactiveUser($uid, $origin);
    }

    /**
     * Lista de assinaturas ativas
     * @param string $origin
     * @return PDOStatement
     */
    public function getListActive($origin) {
        $userdb = $this->getDbMapper();
        return $userdb->getActiveByOrigin($origin);
    }

    /**
     * Lista de todas as assinaturas
     * @param string $origin
     * @return PDOStatement
     */
    public function getListAll($origin) {
        $userdb = $this->getDbMapper();
        return $userdb->getAllByOrigin($origin);
    }

    /**
     * Lista de todas as assinaturas tipo degustacao
     * @param string $origin
     * @return PDOStatement
     */
    public function getAllFormWeb() {
        $userdb = $this->getDbMapper();
        return $userdb->getAllFormWeb();
    }

    /**
     * Lista de todos os assinantes que realizam o login pela primeira vez
     * @return PDOStatement
     */
    public function getListFirstLogin() {
        $userdb = $this->getDbMapper();
        return $userdb->getListFirstLogin();
    }

    /**
     * Desativa todos as assinaturas ativas expiradas
     * @param string $origin
     * @return int
     */
    public function deactiveExpired($origin) {
        $userdb = $this->getDbMapper();
        return $userdb->deactiveExpired($origin);
    }

    /**
     * Lista todas as assinaturas expiradas
     * @param string $origin
     * @return int
     */
    public function getListExpired($origin) {
        $userdb = $this->getDbMapper();
        return $userdb->getListExpired($origin);
    }

}

/**
 * Logger
 */
class Logger {

    protected static $_db = null;
    protected $_logDb = null;

    /**
     * Construtor
     * @param DBAccess $db
     */
    function __construct(DBAccess $db) {
        self::$_db = $db;
    }

    /**
     * Define a conexao ao banco de dados
     * @param DBAccess $db
     */
    public function setDbConnection(DBAccess $db) {
        self::$_db = $db;
    }

    /**
     * Retorna a conexao do banco de dados
     * @return type
     */
    public function getDbConnection() {
        return $this->_db;
    }

    /**
     * Acesso a tabela de log
     * @return LogMapper
     */
    public function getDbMapper() {
        if (!is_object($this->_logDb)) {
            $this->_logDb = new LogMapper();
        }
        return $this->_logDb;
    }

    /**
     * Registro do log
     * @param type $pid        PageID
     * @param type $userID     ID do assinante
     * @param type $message    Mensagem a ser registrada 
     * @return int
     */
    public function log($pid, $userID, $message) {
        $mapper = $this->getDbMapper();
        return $mapper->log($pid, $userID, $message);
    }

}

/**
 * Interface de acesso as operacoes de processamento do arquivo
 */
interface iProcessFileNotifier {

    /**
     * Conte�do da linha a ser extraida
     * @param array $content
     */
    public function getLine(array $content);
}

/**
 * Processa o arquivo de importacao
 */
class ProcessFile {

    protected $_fh;
    protected $_fileName = null;
    protected $_listeners = array();
    protected $fileContent;
    protected $fileContentByLine;
    protected $totalRecordsInformed;
    protected $totalRecordsReaded;
    protected $listUsers;
  
    var $error;
    var $warning;

    /**
     * Construtor
     * @param type $filename
     */
    function __construct($filename) {
        $this->_fileName = $filename;
        $this->error = array();
        $this->warning = array();
        $this->totalRecordsInformed=-1;
        $this->totalRecordsReaded=-1;
        $this->listUsers=array();
    }

    /**
     * Retorna uma lista de observadores
     * @return array
     */
    public function getListeners() {
        return $this->_listeners;
    }

    /**
     * Registra um observador
     * @param iProcessFileNotifier $listener
     */
    public function registerListener($listener) {
        $class = new ReflectionClass($listener);
        if ($class->implementsInterface('iProcessFileNotifier')) {
            $this->_listeners[] = $listener;
        }
    }

    /**
     * Desregistra um observador
     * @param iProcessFileNotifier $listener
     * @return void
     */
    public function unregisterListener($listener) {
        $class = new ReflectionClass($listener);
        if ($class->implementsInterface('iProcessFileNotifier')) {
            foreach ($this->_listeners as $key => $item) {
                if ($item == $listener) {
                    unset($this->_listeners[$key]);
                    return;
                }
            }
        }
    }

    public function readFile($file) {
      $this->fileContent = file_get_contents($file);
      $this->fileContent = preg_replace('/^\h*\v+/m', '', $this->fileContent);
    }

    public function splitFileByLine() {          
      $this->fileContentByLine=explode("\n",$this->fileContent);
    }

  
    public function validateLine($line,$lineNumber) {
      
            if (!empty($line['name'])){
              $USUARIO_NOME=utf8_encode($line['name']);  
            } else {
              $USUARIO_SERIAL=$line['serial'];  
            }
            

            if (empty($line['natureza_juridica'])) {
              throw new ValidatorErrorException("[ERRO] [LINHA:{$lineNumber}] Campo natureza juridica do assinante não informado para {$USUARIO_NOME}",4);                            
            } else if ($line['natureza_juridica'] != 'PF' && $line['natureza_juridica'] != 'PJ') {
              throw new ValidatorErrorException("[ERRO] [LINE:$lineNumber}] Campo natureza juridica deve ser PF ou PJ para {$USUARIO_NOME}",4);                            
            }
      
            if (empty($line['data_expiracao_cartao'])) {
              throw new ValidatorErrorException("[ERRO] [LINHA:{$lineNumber}] Campo data da expiração do cartão não informado para {$USUARIO_NOME}",5);                            
            } else {
              list($dia,$mes,$ano)=explode("/",date("d/m/Y",$line['data_expiracao_cartao']));
              if ( is_numeric($dia) && is_numeric($mes) && is_numeric($ano) ) {
                if (!checkdate($mes,$dia,$ano)){
                 throw new ValidatorErrorException("[ERRO] [LINHA:$lineNumber}] A data de expiração do assinante {$USUARIO_NOME}, está no formato errado, data informada: {$line['data_expiracao_cartao']}.");
                }
              } else {
                 throw new ValidatorErrorException("[ERRO] [LINHA:$lineNumber}] A data de expiração do assinante {$USUARIO_NOME}, está no formato errado, data informada: {$line['data_expiracao_cartao']}.");
              }      
            }
      
      
            if (!empty($line['email']) && !filter_var($line['email'], FILTER_VALIDATE_EMAIL)) {
                throw new ValidatorWarningException("[ATENCAO] [LINHA:{$lineNumber}] Campo email informado para {$USUARIO_NOME} não é um e-mail válido: {$line['email']}",0);
            }
      
            if (empty($line['cpf'])) {
              throw new ValidatorWarningException("[ATENCAO] [LINHA:{$lineNumber}] Campo CPF/CNPJ não informado para {$USUARIO_NOME}",0); 
            }
                
            if (empty($line['serial'])) {
              throw new ValidatorErrorException("[ERRO] [LINHA:{$lineNumber}] Campo código de assinante não informado para usuário {$USUARIO_NOME}",1);
            }
                
            if (empty($line['name'])) {
              throw new ValidatorErrorException("[ERRO] [LINHA:{$lineNumber}] Campo nome do assinante não informado para usuário ID: {$USUARIO_SERIAL}",2);                            
            }

            if (!is_numeric($line['flag_clubecorreio']) || ($line['flag_clubecorreio'] != '0' && $line['flag_clubecorreio'] != '1')) {
              throw new ValidatorErrorException("[ERRO] [LINHA:{$lineNumber}] Campo 'Clube Correio' não informado para usuário: {$USUARIO_NOME}",2);                            
            }
      
      
            return true;

    }
  
     function getDataFromTimestamp($data){
       $time='';
       if (strpos($data,"/") !== false) {
         list($dia,$mes,$ano) = explode("/",$data);
         $time = mktime(0,0,0,$mes,$dia,$ano);
       }
       
       return $time;
     }
  
     function getDataFromLine($line) {
       
        list($cpf, $serial, $name, $email, $natureza_juridica, $nome_responsavel_exibicao_cartao, $data_expiracao_cartao,$cod_campanha, $nome_campanha,$flag_clubecorreio) = explode(";", $line);
       
        $data = array();
        $data['cpf'] = trim($cpf);
        $data['serial'] = (int) trim($serial);
        $data['name'] = trim($name);
        $data['email'] = trim($email);
        $data['natureza_juridica'] = trim($natureza_juridica);
       
        $data_expiracao_cartao = $this->getDataFromTimestamp($data_expiracao_cartao);
        
       
        $data['data_expiracao_cartao'] = $data_expiracao_cartao;
        $data['nome_responsavel_exibicao_cartao'] = trim($nome_responsavel_exibicao_cartao);
        $data['flag_clubecorreio'] = (int) trim($flag_clubecorreio);
      
        return $data;
     }
  
     function showErrors() {
        if (count($this->error) > 0) {
          foreach($this->error as $idx => $err) {
              echo $err,"\n";              
          }      
        }       
     }

     function showWarning() {
        if (count($this->warning) > 0) {
          foreach($this->warning as $idx => $warning) {
              echo $warning,"\n";     
          }      
        }       
     }
  
    function getLastLineInformed($line){
    
      $key    = substr($line,0,1);
      $number = (int) substr($line,1);

      if ($key == 'R'){
        $this->totalRecordsInformed = $number;
        return true;
      }

    return false;
  }

  
   function validateUniqueRecord($serial,$cpf,$nome,$lineNumber){
     if (!isset($this->listUsers[$serial][$cpf])){
       
       $this->listUsers[$serial][$cpf]=$lineNumber;
       
     } else {
       
       $nome=utf8_encode($nome);
       
       throw new ValidatorWarningException("[ATENCAO] [LINHA:{$lineNumber}] Encontrado registro duplicado na linha {$this->listUsers[$serial][$cpf]} para o assinante: {$nome}",0);
       
     }
     return true;
   }
  
    /**
     * Abre o arquivo e le linha a linha
     * @throws Exception
     * @return int
     */
    public function run() {

            $this->_fh = fopen($this->_fileName, "r");
            if (!$this->_fh) {
                throw new Exception("\033[0;31mNao foi possivel abrir o arquivo " . $this->_fileName . "." . "\033[0m" . PHP_EOL);
            }

      
            $this->readFile($this->_fileName); 
            $this->splitFileByLine();
      
            $lineFile = 1;
            foreach($this->fileContentByLine as $line) {
                if (strlen($line) == 0) {
                    $lineFile++;
                    continue;
                }

                // Ignore a primeira linha
                if ($lineFile == 1) {
                    $lineFile++;
                    continue;
                }

                // Ignore a ultima linha
                if (substr($line, 0, 1) == 'R') {
                    //$totalLinhas=(int) substr($buffer,1,strlen($buffer));
                    $this->getLastLineInformed($line);
                    $lineFile++;
                    continue;
                }
              
                $data=$this->getDataFromLine($line);
                
                try {
                  
                  $this->validateLine($data,$lineFile);   
                  
                  $this->validateUniqueRecord($data['serial'],$data['cpf'],$data['name'],$lineFile);
                  
                  
                } catch (ValidatorWarningException $e) {
                  
                  $this->warning[]=$e->getMessage();
                  
                } catch (ValidatorErrorException $e) {
                  
                  $this->error[]=$e->getMessage();
                  
                }
              
                $lineFile++;
              
            }

          unset($this->listUsers);
          $this->totalRecordsReaded=$lineFile - 3;
      
      
      
          if ($this->totalRecordsReaded != $this->totalRecordsInformed || $this->totalRecordsReaded == -1 || $this->totalRecordsInformed == -1) {
            $this->error[]="[ERRO] Total de registros informados, {$this->totalRecordsInformed}, diferente do total de registros lidos {$this->totalRecordsReaded}.";
          }
      
          if (count($this->error) == 0) {
            $lineFile=1;
            foreach($this->fileContentByLine as $line) {
              
                if (strlen($line) == 0) {
                    $lineFile++;
                    continue;
                }

                // Ignore a primeira linha
                if ($lineFile == 1) {
                    $lineFile++;
                    continue;
                }

                // Ignore a ultima linha
                if (substr($line, 0, 1) == 'R') {
                    $this->getLastLineInformed($line);
                    continue;
                }
              
                $data=$this->getDataFromLine($line);
                $listeners = $this->getListeners();            
                foreach ($listeners as $item) {
                    if (is_object($item)) {
                       $item->getLine($data);
                    }
                }
              
                $lineFile++;
              
            }
          } else {     
            
            if (count($this->warning) != 0) {
              $this->showWarning();
            } 
            
            $this->showErrors();
            $msg  ="-------------------------------------------------------------------------------------\n\n";
            $msg .="[ERRO] Nenhuma informação foi atualizada ou inserida. O arquivo contém erros críticos.\n\n";
            $msg .="-------------------------------------------------------------------------------------\n\n";
            throw new Exception ($msg,0);
          }

         if (count($this->warning) != 0) {
            $this->showWarning();
            echo "------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
            echo "[ATENCAO] A importação foi realizada com as ressalvas.\n\nÉ recomendavel que os alertas informados sejam tratados, não tratar os alertas pode impactar em comportamento não previsível das aplicações que usam os dados fornecidos pelo arquivo de importação.\n\n";
            echo "------------------------------------------------------------------------------------------------------------------------------------------------\n\n"; 
          } 
      
        return $lineFile;
    }
}
  
  

/**
 * Processa o arquivo de importacao e grava as alteracoes em banco de dados
 */
class ImportSubscribers implements iProcessFileNotifier {

    /**
     * Acesso ao banco de dados 
     * @var DBAccess
     */
    protected static $_db;

    /**
     * Nome da origem
     * @var string
     */
    protected $_origin = "";

    /**
     * PageID
     * @var string
     */
    protected $_pid = null;

    /**
     * Lista de assinaturas antes da modificacao
     * @var array
     */
    protected $_actualSubscribers = array();

    /**
     * Lista auxiliar de assinaturas antes da modificacao
     * @var array
     */
    protected $_actualSubscribers2 = array();

    /**
     * Lista auxiliar de assinaturas de origem FormWeb
     * @var array
     */
    protected $_subscribersFormWeb = array();

    /**
     * Lista de assinaturas presentes no arquivo importado
     * @var array
     */
    protected $_newSubscribers = array();

    /**
     * Acesso a tabela de assinaturas
     * @var Subscribers 
     */
    protected $_subscribers = null;

    /**
     * Acesso a tabela de log
     * @var Logger
     */
    protected $_logger = null;

    /**
     * Quantidade de assinaturas lidas no arquivo
     * @var int
     */
    protected $_readLines = 0;

    /**
     * Contadores de operacoes realizadas
     * @var Statistics
     */
    protected $_stats = null;

    /**
     * Construtor
     * @param DBAccess $db
     * @param string $pid
     */
    function __construct($db, $pid) {
        self::$_db = $db;
        $this->_pid = $pid;
        $this->_getSubscribers();
        $this->_stats = new Statistics();
    }

    /**
     * Acesso a tabela de assinaturas
     * @return Subscribers
     */
    protected function _getSubscribers() {
        if (!is_object($this->_subscribers)) {
            $this->_subscribers = new Subscribers(self::$_db);
        }
        return $this->_subscribers;
    }

    /**
     * Obtem os contadores das operacoes realizadas
     * @return Statistics
     */
    public function getStats() {
        if (!is_object($this->_stats)) {
            $this->_stats = new Statistics();
        }
        return $this->_stats;
    }

    /**
     * Define o objeto de acesso aos dados das assinaturas
     * @param Subscribers $value
     */
    public function setSubscribers($value) {
        $this->_subscribers = $value;
    }

    /**
     * Retorna o objeto de logger
     * @return Logger
     */
    protected function _getLogger() {
        if (!is_object($this->_logger)) {
            $this->_logger = new Logger(self::$_db);
        }
        return $this->_logger;
    }

    /**
     * Define o objeto de logger
     * @param Logger $value
     */
    public function setLogger($value) {
        $this->_logger = $value;
    }

    /**
     * Adiciona uma nova assinatura
     * 
     * @param string $pid      PageID
     * @param string $name     Nome do assinante
     * @param string $email    E-mail do assinante 
     * @param string $login    login do Assinante
     * @param string $password Senha do Assinante
     * @param string $cpf      CPF do assinante
     * @param string $serial   ID do sistema da assinatura
     * @param string $origin   Tipo de cadastro
     * 
     * @return int
     */
    public function createNewSubscriber($cpf, $serial, $name, $email, $origin,$natureza_juridica,$data_expiracao_cartao,$nome_responsavel_exibicao_cartao,$flag_clubecorreio) {
        $login = $cpf;
        $password = $cpf;

        $subscribers = $this->_getSubscribers();
        return $subscribers->createNew($this->_pid, $name, $email, $login, $password, $cpf, $serial, $origin,$natureza_juridica,$data_expiracao_cartao,$nome_responsavel_exibicao_cartao,$flag_clubecorreio);
    }

    /**
     * Altera o e-mail do assinante
     * 
     * @param string $email  E-mail do assinante 
     * @param string $cpf    CPF do assinante
     * @param string $serial ID do sistema da assinatura
     * 
     * @return int
     */
    public function changeEmailSubscriber($email, $cpf, $serial) {
        $subscribers = $this->_getSubscribers();
        return $subscribers->changeEmail($email, $cpf, $serial);
    }
  
    public function changeFlagClubeCorreioSubscriber($flag_clubecorreio, $cpf, $serial) {
        $subscribers = $this->_getSubscribers();
        return $subscribers->changeFlagClubeCorreio($flag_clubecorreio, $cpf, $serial);
    }  

    public function changeDataExpiracaoCartaoSubscriber($data_expiracao, $cpf, $serial) {
        $subscribers = $this->_getSubscribers();
        return $subscribers->changeDataExpiracaoCartao($data_expiracao, $cpf, $serial);
    }

    public function changeNaturezaJuridicaSubscriber($natureza_juridica, $cpf, $serial) {
        $subscribers = $this->_getSubscribers();
        return $subscribers->changeNaturezaJuridica($natureza_juridica, $cpf, $serial);
    }

    public function changeNomeResponsavelExibicaoCartaoSubscriber($nome_responsavel_exibicao_cartao, $cpf, $serial) {
        $subscribers = $this->_getSubscribers();
        return $subscribers->changeNomeResponsavelExibicaoCartao($nome_responsavel_exibicao_cartao, $cpf, $serial);
    }
  
  
    /**
     * Ativa uma assinatura
     * 
     * @param string $cpf
     * @param string $serial
     * 
     * @return int
     */
    public function activeSubscriber($cpf, $serial) {
        $subscribers = $this->_getSubscribers();
        return $subscribers->active($cpf, $serial);
    }

    /**
     * Obtem a lista de todos os assinantes que realizaram o primeiro login
     * 
     * @return PDOStatement
     */
    public function getListFirstLogin() {
        $subscribers = $this->_getSubscribers();
        return $subscribers->getListFirstLogin();
    }

    /**
     * Formata CPF/CNPJ para o formato presente do arquivo 
     * 
     * @param string $cpf 
     */
    protected function _clearCPFCNPJ($cpf) {
        $cpf = preg_replace('/[^0-9]/i', '', $cpf);
        if (strlen($cpf) > 11) {
            $cpf = substr('00000000000000' . $cpf, -14);
        } else {
            $cpf = substr('00000000000' . $cpf, -11);
        }
        return $cpf;
    }

    /**
     * Desative todos os assinantes presentes antes da atualizacao
     * 
     * @param $origin Tipo de cadastro
     */
    public function deactiveSubscribers($origin) {
        $subscribers = $this->_getSubscribers();

        if (isset($this->_actualSubscribers) && is_array($this->_actualSubscribers)) {
            reset($this->_actualSubscribers);

            foreach ($this->_actualSubscribers as $serial => $arrayCPF) {
                foreach ($arrayCPF as $cpf => $valObj) {
                    $uid = $valObj->uid;
                    $name = $valObj->name;
                    // Se não existir no arquivo de importacao, desative 
                    if (!isset($this->_newSubscribers[$serial][$cpf]) && $origin == 'VAREJO') {
                        if ($subscribers->deactive($uid, $origin) > 0) {
                            $this->getStats()->add('DEACTIVE', $name);
                        }
                    }
                }
            }
//            while (list($serial, $arrayCPF) = each($this->_actualSubscribers)) {
//                while (list($cpf, $valObj) = each($arrayCPF)) {
//                    $uid = $valObj->uid;
//                    $name = $valObj->name;
//                    // Se n�o existir no arquivo, desative 
//                    if (!isset($this->_newSubscribers[$serial][$cpf])) {
//                        if ($subscribers->deactive($uid, $origin) > 0) {
//                            $this->getStats()->add('DEACTIVE', $name);
//                        }
//                    }
//                }
//            }
        }
    }

    /**
     * Processa a linha atual do arquivo
     * 
     * @param string $cpf      CPF do assinante
     * @param string $serial   ID do sistema da assinatura
     * @param string $name     Nome do assinante
     * @param string $email    E-mail do assinante 
     * @param string $origin   Tipo de cadastro
     */
    public function processLine($cpf, $serial, $name, $email,$natureza_juridica,$data_expiracao_cartao,$nome_responsavel_exibicao_cartao,$flag_clubecorreio) {

        $logger = $this->_getLogger();

        if (!isset($this->_newSubscribers[$serial])) {          
            $this->_newSubscribers[$serial][$cpf] = $email;
        }

      
        if (!isset($this->_actualSubscribers2[$serial][$cpf])) {

            if (!isset($this->_actualSubscribers[$serial][$cpf])) {
                
                if ($this->createNewSubscriber($cpf, $serial, $name, $email, $this->_origin,$natureza_juridica,$data_expiracao_cartao,$nome_responsavel_exibicao_cartao,$flag_clubecorreio) > 0) {
                    $this->getStats()->add('ADD', $name);
                }
            } else {



                $objUser = $this->_actualSubscribers[$serial][$cpf];

                $email1 = strtolower(trim($email));
                $email2 = strtolower(trim($objUser->email));

                // emails diferentes ? atualize
                if (strcasecmp($email1, $email2) != 0) {
                    if ($this->changeEmailSubscriber($email, $cpf, $serial) > 0) {
                        $this->getStats()->add('UPDATE', $name);
                    }
                }
              
                // comparando flag_clubecorreio
                $flagCC1 = intval($flag_clubecorreio);
                $flagCC2 = intval($objUser->flag_clubecorreio);
                if ($flagCC1 != $flagCC2) {
                    if ($this->changeFlagClubeCorreioSubscriber($flag_clubecorreio, $cpf, $serial) > 0) {
                        $this->getStats()->add('UPDATE', $name);
                    }
                }
              

                $nj1 = strtolower(trim($natureza_juridica));
                $nj2 = strtolower(trim($objUser->natureza_juridica));
                if (strcasecmp($nj1, $nj2) != 0) {
                    if ($this->changeNaturezaJuridicaSubscriber($natureza_juridica, $cpf, $serial) > 0) {
                        $this->getStats()->add('UPDATE', $name);
                    }
                }

                $de1 = $data_expiracao_cartao;
                $de2 = $objUser->data_expiracao_cartao;              
              
                if ( $de1 != $de2 ) {                
                    if ($this->changeDataExpiracaoCartaoSubscriber($data_expiracao_cartao, $cpf, $serial) > 0) {
                        $this->getStats()->add('UPDATE: ', $name);
                    }
                }

              
                $no1 = strtolower(trim($nome_responsavel_exibicao_cartao));
                $no2 = strtolower(trim($objUser->nome_responsavel_exibicao_cartao));

                // emails diferentes ? atualize
                if (strcasecmp($no1, $no2) != 0) {
                    if ($this->changeNomeResponsavelExibicaoCartaoSubscriber($nome_responsavel_exibicao_cartao, $cpf, $serial) > 0) {
                        $this->getStats()->add('UPDATE', $name);
                    }
                }

                $nj1 = strtolower(trim($natureza_juridica));
                $nj2 = strtolower(trim($objUser->natureza_juridica));
                if (strcasecmp($nj1, $nj2) != 0) {
                    if ($this->changeNaturezaJuridicaSubscriber($natureza_juridica, $cpf, $serial) > 0) {
                        $this->getStats()->add('UPDATE', $name);
                    }
                }
              
              
              
                // Assinante desativado/apagado ? Ative novamente.
                if ($objUser->hidden == 1 || $objUser->deleted == 1) {
                    if ($this->activeSubscriber($cpf, $serial) > 0) {
                        $this->getStats()->add('ACTIVE', $name);
                        if (isset($this->_subscribersFormWeb[$cpf])) {
                            $this->getStats()->add('CONVERSAO-DEGUSTACAO', $objUser->name);
                            $logger->log($this->_pid, $objUser->uid, "conversao-assinatura-degustacao-varejo");
                        }
                    }
                } 
            }
        }
    }

    /**
     * Obtem uma linha do arquivo e o processa
     * @param array $content Linha do arquivo
     */
    public function getLine(array $content) {

        $cpf = $this->_clearCPFCNPJ($content['cpf']);
        $serial = $content['serial'];
        $name = $content['name'];
        $email = $content['email'];
        $natureza_juridica = $content['natureza_juridica'];
        $data_expiracao_cartao = $content['data_expiracao_cartao'];
        $nome_responsavel_exibicao_cartao = $content['nome_responsavel_exibicao_cartao'];
        $flag_clubecorreio = $content['flag_clubecorreio'];

        if (($cpf * 1) == 0) {
            $this->getStats()->add('INVALIDCPF', array(utf8_encode($name), $serial));
        } else {          
          $this->processLine($cpf, $serial, $name, $email,$natureza_juridica,$data_expiracao_cartao,$nome_responsavel_exibicao_cartao,$flag_clubecorreio);
        }

        $this->_readLines++;
        $this->getStats()->set('READLINES', $this->_readLines);
    }

    /**
     * Obtem a lista de todos os assinantes atuais do banco de dados
     * 
     * @param string $origin Tipo de cadastro
     * @param boolean $force Forca que a lista de assinaturas atuais seja recarregada
     * @return array
     */
    public function getListSubscribers($origin, $force = false) {

        if (!$force && is_array($this->_actualSubscribers) && (count($this->_actualSubscribers) > 0)) {
            return $this->_actualSubscribers;
        }

        $this->_actualSubscribers = array();
        $subscribers = $this->_getSubscribers();
        $result = $subscribers->getListAll($origin);
        if ($result == null) {
            return array();
        }
        foreach ($result as $row) {
            if (isset($row['id_numerico_rb']) && (strlen($row['id_numerico_rb']) > 0)) {
                $cpf = $this->_clearCPFCNPJ($row['cpf']);

                $object = new stdClass();
                $object->cpf = $cpf;
                $object->serial = $row['id_numerico_rb'];
                $object->name = $row['nome'];
                $object->email = $row['email'];
                $object->uid = $row['uid'];
                $object->hidden = $row['hidden'];
                $object->deleted = $row['deleted'];
                $object->origin = $row['origem'];
                $object->natureza_juridica = trim($row['natureza_juridica']);
                $object->data_expiracao_cartao = trim($row['data_expiracao_cartao']);
                $object->nome_responsavel_exibicao_cartao = trim($row['nome_responsavel_exibicao_cartao']);
                $object->flag_clubecorreio = trim($row['flag_clubecorreio']);

                $this->_actualSubscribers[$row['id_numerico_rb']][$cpf] = $object;
            }
        }

        return $this->_actualSubscribers;
    }

    /**
     * Caso especial de cadastro vir do formulario
     * 
     * @return array
     */
    public function getListSubscribersFormWeb() {

        $this->_subscribersFormWeb = array();
        $subscribers = $this->_getSubscribers();
        $result = $subscribers->getAllFormWeb();
        if ($result == null) {
            return array();
        }
        foreach ($result as $row) {
            $cpf = $this->_clearCPFCNPJ($row['cpf']);

            $object = new stdClass();
            $object->cpf = $cpf;
            $object->name = trim($row['nome']);
            $object->email = trim($row['email']);
            $object->uid = $row['uid'];
            $object->hidden = $row['hidden'];
            $object->deleted = $row['deleted'];
            $object->natureza_juridica = trim($row['natureza_juridica']);
            $object->data_expiracao_cartao = trim($row['data_expiracao_cartao']);
            $object->nome_responsavel_exibicao_cartao = trim($row['nome_responsavel_exibicao_cartao']);
            $object->flag_clubecorreio = intval($row['flag_clubecorreio']);
          

            $this->_subscribersFormWeb[$cpf] = $object;
        }

        return $this->_subscribersFormWeb;
    }

    /**
     * Obtem a lista de todos os assinantes ativos do banco de dados
     * 
     * @param string $origin Tipo de cadastro
     * @param boolean $force Forca que a lista de assinaturas ativas seja recarregada
     * @return array
     */
    public function getListActiveSubscribers($origin, $force = false) {

        if (!$force && is_array($this->_actualSubscribers2) && (count($this->_actualSubscribers2) > 0)) {
            return;
        }

        $this->_actualSubscribers2 = array();
        $subscribers = $this->_getSubscribers();
        $result = $subscribers->getListActive($origin);
        if ($result == null) {
            return array();
        }
        foreach ($result as $row) {
            if (isset($row['id_numerico_rb']) && (strlen($row['id_numerico_rb']) > 0)) {
                $cpf = $this->_clearCPFCNPJ($row['cpf']);

                $object = new stdClass();
                $object->cpf = $cpf;
                $object->serial = $row['id_numerico_rb'];
                $object->name = trim($row['nome']);
                $object->email = trim($row['email']);
                $object->uid = $row['uid'];
                $object->natureza_juridica = trim($row['natureza_juridica']);
                $object->data_expiracao_cartao = trim($row['data_expiracao_cartao']);
                $object->nome_responsavel_exibicao_cartao = trim($row['nome_responsavel_exibicao_cartao']);
                $object->flag_clubecorreio = intval($row['flag_clubecorreio']);

                $this->_actualSubscribers2[$row['id_numerico_rb']][$cpf] = $object;
            }
        }

        return $this->_actualSubscribers2;
    }

    /**
     * Desativa todos os assinanturas-degustacao cuja a assinatura esteja expirada
     * 
     * @param type $origin Tipo de Cadastro
     */
    public function updateExpired($origin) {

        $subscribers = $this->_getSubscribers();

        if (strtoupper($origin) == 'FORMWEB') {

            $logger = $this->_getLogger();
            $list = $subscribers->getListExpired($origin);
            if ($list == null) {
                return;
            }
            foreach ($list as $item) {

                $userID = $item['uid'];
                $name = $item['nome'];
                $logger->log($this->_pid, $userID, "congelamento-degustacao");
                $this->getStats()->add('CONGELAMENTO-DEGUSTACAO', $name);
            }
        }

        $subscribers->deactiveExpired($origin);
    }

    /**
     * Realiza todos as operacoes relativas ao arquivo de importacao
     * 
     * @param type $filename Nome do arquivo
     * @param type $origin   Tipo de Cadastro 
     * @param type $origin2  Tipo de Cadastro a se considerar.Ex: CORPORATIVO
     */
    public function run($filename, $origin, $origin2 = '') {

        $this->_origin = strtoupper(trim($origin));


        $this->getListSubscribers($this->_origin);
        $this->getListSubscribersFormWeb();

        if ($origin2 != '') {
            $this->getListActiveSubscribers($origin2);
        } else {
            $this->_actualSubscribers2 = array();
        }
        
        
        $processFile = new ProcessFile($filename);
        $processFile->registerListener($this);

        $processFile->run();
      
        // DESCOMENTAR ESSAS LINHAS
        $this->deactiveSubscribers($origin);
        $this->updateExpired('FormWeb');
    }

}

/**
 *  Gera o relatorio a partir das estatisticas
 */
class Report {

    const RENDER_NOCPF = 1;
    const RENDER_SUBSCRIBERSADD = 2;
    const RENDER_SUBSCRIBERDEACTIVE = 4;
    const RENDER_SUBSCRIBERACTIVE = 8;
    const RENDER_FREEZEPROMOTIONALSUBSCRIPTION = 16;
    const RENDER_CONVERSIONPROMOTIONALSUBSCRIPTION = 32;
    const RENDER_TOTALS = 64;
    const RENDER_ALL = 127;

    /**
     *
     * @var Statistics
     */
    private $_stats = null;

    function __construct($stats) {
        $this->_stats = $stats;
    }

    /**
     * Get Statistics
     * @return Statistics
     */
    public function getStats() {
        return $this->_stats;
    }

    /**
     * Define as Statistics
     * @param Statistics $value
     */
    public function setStats($value) {
        $this->_stats = $value;
    }

    /**
     * Lista de assinantes com CPF invalido
     * @return string
     */
    protected function _getPartNoCPF() {
        $stats = $this->getStats();
        $array = $stats->get('INVALIDCPF');
        if (!is_array($array)) {
            return '';
        }
        $result = "";
        foreach ($array as $nocpf) {
            $result.= " Cod. Assinante: ". $nocpf[1] . " -> " . $nocpf[0] . ".\n";
        }
        return $result;
    }

    /**
     * Lista de novas assinaturas 
     * @return string
     */
    protected function _getPartSubscribersAdd() {
        $stats = $this->getStats();
        $array = $stats->get('ADD');
        if (!is_array($array)) {
            return '';
        }
        $result = "";
        foreach ($array as $name) {
            $result.= " {$name}\n";
        }
        return $result;
    }

    /**
     * Lista de assinaturas desativadas
     * @return string
     */
    protected function _getPartSubscriberDeactive() {
        $stats = $this->getStats();
        $array = $stats->get('DEACTIVE');
        if (!is_array($array)) {
            return '';
        }
        $result = "";
        foreach ($array as $name) {
            $result.= " {$name}\n";
        }
        return $result;
    }

    /**
     * Lista de assinaturas reativadas
     * @return string
     */
    protected function _getPartSubscriberActive() {
        $stats = $this->getStats();
        $array = $stats->get('ACTIVE');
        if (!is_array($array)) {
            return '';
        }
        $result = "";
        foreach ($array as $name) {
            $result.= " {$name}\n";
        }
        return $result;
    }

    /**
     * Lista de assinaturas promocionais congeladas
     * @return string
     */
    protected function _getPartFreezePromotionalSubscription() {
        $stats = $this->getStats();
        $array = $stats->get('CONGELAMENTO-DEGUSTACAO');
        if (!is_array($array)) {
            return '';
        }
        $result = "";
        foreach ($array as $name) {
            $result.= " {$name}\n";
        }
        return $result;
    }

    /**
     * Lista de assinaturas promocionais convertidas para assinaturas convencionais
     * @return string
     */
    protected function _getPartConversionPromotionalSubscription() {
        $stats = $this->getStats();
        $array = $stats->get('CONVERSAO-DEGUSTACAO');
        if (!is_array($array)) {
            return '';
        }
        $result = "";
        foreach ($array as $name) {
            $result.= " {$name}\n";
        }
        return $result;
    }

    /**
     * Total de linhas processadas
     * @return string
     */
    protected function _getPartReadLines() {
        $stats = $this->getStats();
        return $stats->get('READLINES');
    }

    /**
     * Obter resumo (Totais)
     * @return string
     */
    protected function _getPartTotals() {

        $stats = $this->getStats();
        $logADD = $stats->get('ADD');
        $logDEL = $stats->get('DEACTIVE');
        $logUPD = $stats->get('UPDATE');
        $logDES = $stats->get('ACTIVE');
        $logCONG = $stats->get('CONGELAMENTO-DEGUSTACAO');
        $logCONV = $stats->get('CONVERSAO-DEGUSTACAO');
        $logINVALIDCPF = $stats->get('INVALIDCPF');
        $totalLinhas = $stats->get('READLINES');

        $content = "";
        if (isset($logINVALIDCPF) && is_array($logINVALIDCPF) && count($logINVALIDCPF) >= 1) {
            $content.= "Total sem CPF: " . count($logINVALIDCPF) . "\n";
        }
        if (isset($logADD) && is_array($logADD) && count($logADD) >= 1) {
            $content.= "Total adicionados: " . count($logADD) . "\n";
        }

        if (isset($logDEL) && is_array($logDEL) && count($logDEL) >= 1) {
            $content.= "Total congelados: " . count($logDEL) . "\n";
        }

        if (isset($logDES) && is_array($logDES) && count($logDES) >= 1) {
            $content.= "Total descongelados: " . count($logDES) . "\n";
        }

        if (isset($logUPD) && is_array($logUPD) && count($logUPD) >= 1) {
            $content.= "Total atualizados: " . count($logUPD) . "\n";
        }

        if (isset($logCONG) && is_array($logCONG) && count($logCONG) >= 1) {
            $content.= "Total degustacao congelamento: " . count($logCONG) . "\n";
        }

        if (isset($logCONV) && is_array($logCONV) && count($logCONV) >= 1) {
            $content.= "Total degustacao conversao para VAREJO: " . count($logCONV) . "\n";
        }

        $content.= "Total linhas lidas: " . ($totalLinhas + 2) . "\n";
        $content.= "Total linhas informadas: " . $totalLinhas . "\n";
        return $content;
    }

    /**
     * Obter data/hora atual
     * @return string
     */
    protected function _getDatetime() {
        return date('d/m/Y h:i:s');
    }

    /**
     * Constroe o relatorio
     * @param int $part
     * @return string
     */
    protected function _buildReport($part = self::RENDER_ALL) {
        $datetime = $this->_getDatetime();
        $partNoCPF = $this->_getPartNoCPF();
        $partSubscribersAdd = $this->_getPartSubscribersAdd();
        $partSubscriberDeactive = $this->_getPartSubscriberDeactive();
        $partSubscriberActive = $this->_getPartSubscriberActive();
        $partFreezePromotionalSubscription = $this->_getPartFreezePromotionalSubscription();
        $partConversionPromotionalSubscription = $this->_getPartConversionPromotionalSubscription();
        $partTotal = $this->_getPartTotals();

        $content = "";
        
        if ($part & self::RENDER_NOCPF) {
            $content.= "\n\nCPF/CPNJ zerados \n";
            $content.= "--------------------------------------------------------------------------------\n\n";
            if ($partNoCPF != '') {
                $content.= $partNoCPF;
            } else {
                $content.= "Sem resultados.\n";
            }
        }
        if ($part & self::RENDER_SUBSCRIBERSADD) {
            $content.= "\n\nNovas assinaturas \n";
            $content.= "--------------------------------------------------------------------------------\n\n";
            if ($partSubscribersAdd != '') {
                $content.= $partSubscribersAdd;
            } else {
                $content.= "Sem resultados.\n";
            }
        }
        #if ($part & self::RENDER_SUBSCRIBERDEACTIVE) {
        #    $content.= "\n\nAssinaturas desativadas \n";
        #    $content.= "--------------------------------------------------------------------------------\n\n";
        #    if ($partSubscriberDeactive != '') {
        #        $content.= $partSubscriberDeactive;
        #    } else {
        #        $content.= "Sem resultados.\n";
        #    }
        #}
        if ($part & self::RENDER_SUBSCRIBERACTIVE) {
            $content.= "\n\nAssinaturas ativadas\n";
            $content.= "--------------------------------------------------------------------------------\n\n";
            if ($partSubscriberActive != '') {
                $content.= $partSubscriberActive;
            } else {
                $content.= "Sem resultados.\n";
            }
        }
        if ($part & self::RENDER_FREEZEPROMOTIONALSUBSCRIPTION) {
            $content.= "\n\nAssinaturas promocionais congeladas: \n";
            $content.= "--------------------------------------------------------------------------------\n\n";
            if ($partFreezePromotionalSubscription != '') {
                $content.= $partFreezePromotionalSubscription;
            } else {
                $content.= "Sem resultados.\n";
            }
        }
        if ($part & self::RENDER_CONVERSIONPROMOTIONALSUBSCRIPTION) {
            $content.= "\n\nAssinaturas promocionais convertidas para o VAREJO: \n";
            $content.= "--------------------------------------------------------------------------------\n\n";
            if ($partConversionPromotionalSubscription != '') {
                $content.= $partConversionPromotionalSubscription;
            } else {
                $content.= "Sem resultados.\n";
            }
        }
        if ($part & self::RENDER_TOTALS) {
            $content.= "\n\nResumo \n";
            $content.= "--------------------------------------------------------------------------------\n\n";
            $content.= $partTotal;
        }

        return $content;
    }

    /**
     * Exibe na tela o relatorio
     * @param int $part
     */
    public function render($part = self::RENDER_ALL) {
        echo $this->_buildReport($part);
    }

    /**
     * Exporta o conteudo do relatorio para um arquivo
     * @param string $filename
     * @param string $part
     */
    public function exportToFile($filename, $part = self::RENDER_ALL) {
        $content = $this->_buildReport($part);
        file_put_contents($filename, $content);
    }

}

class Console {

    static public function showHelp() {
        $programname = basename(__FILE__);
        $text = <<<EOT
Uso: php {$programname} <VAREJO|CORPORATIVO> <ARQUIVO_A_IMPORTAR>


EOT;
        echo $text;
    }

    static function processParameters($argv) {
        // print_r($argv);
        $result = array();
        if (count($argv) == 3) {
            $origin = strtoupper($argv[1]);
            $filename = $argv[2];

            if (!in_array($origin, array('VAREJO', 'CORPORATIVO'))) {
                echo "\033[0;31m\nOrigem somente pode ser do tipo 'VAREJO' ou 'CORPORATIVO'.\n\033[0m" . PHP_EOL;
                self::showHelp();
                return false;
            }

            if (!file_exists($filename)) {
                echo "\033[0;31m\nArquivo a importar nao encontrado: $filename.\n\033[0m" . PHP_EOL;
                self::showHelp();
                return false;
            }

            $result['origin'] = $origin;
            $result['file'] = $filename;
        } else {
            self::showHelp();
            return false;
        }
        return $result;
    }

}

function fileWasProcessed($importfile) {

    $filename = 'importa_assinantes_md5files.txt';
    $calcmd5 = md5_file($importfile);

    if (!file_exists($filename)) {
        file_put_contents($filename, $calcmd5);
        return false;
    }

    $filemd5 = file_get_contents($filename);
    if ($calcmd5 == trim($filemd5)) {
        return true;
    }
}

function registerFileAsProcessed($importfile) {
    $filename = 'importa_assinantes_md5files.txt';
    $calcmd5 = md5_file($importfile);
    file_put_contents($filename, $calcmd5);
}


/*

  INICIO DO SCRIPT
  
*/

//VAREJO
//CORPORATIVO
//Comercial
//Diretoria
//CORTESIAS
//FormWeb

$params = Console::processParameters($argv);
if (!is_array($params)) {
    exit(1);
}

$origin = $params['origin'];
$importfile = $params['file'];

$db = new DBAccess(DBHOST, DBNAME, DBUSER, DBPASS);

$import = new ImportSubscribers($db, PID);

$datetime=date("d/m/Y H:i:s");

echo  "\n###########################################################\n";
echo  "########    IMPORTAÇÃO EM {$datetime}   ###########\n";
echo  "###########################################################\n\n";


try {
  
  ob_start();
  echo  "\n###########################################################\n";
  echo  "############## RELATÓRIO DE VALIDAÇAO DE ARQUIVO ##########\n";
  echo  "###########################################################\n\n";
  
  $import->run($importfile, $origin);   
  $validationData = ob_get_contents();
  ob_end_clean();
  
  
  
  registerFileAsProcessed($importfile);
  $firstLoginCount = $import->getListFirstLogin();

  if (SHOWLOG) {
    
    $stats = $import->getStats();
    $report = new Report($stats);
    $report->render();
    //$report->exportToFile('importacao_' . date('Ymdhis') . '.log');
    echo "Total Primeiro Login: " . $firstLoginCount . "\n";
    
    echo $validationData;
  }
  
} catch (Exception $e) {
  echo "\n";
  $msg = $e->getMessage();
  
  echo $msg;
  
}
