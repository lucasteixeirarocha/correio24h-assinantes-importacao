#!/usr/bin/python

import sys
import time

def main(argv):       
  #print 'Arquivos para Concatenar:', str(argv)         
  now = time.strftime("%H%M%S")
  outputFile=argv[0] + 'assinantes'+'.txt'
  filenames = argv[1:]
  totalRecords=0;
  with open(outputFile, 'w') as outfile:
    fileNumber=1
    for fname in filenames:
      with open(fname) as infile:
          lineNumber=1;
          for line in infile:
              if (lineNumber == 1 and fileNumber == 1):
                outfile.write(line)
              elif (lineNumber != 1):
                lineType=line.find('R')
                if (lineType!=0):
                  outfile.write(line)
                else:  
                  totalRecords=totalRecords + int(line[1:])
                
              lineNumber=lineNumber + 1
      fileNumber=fileNumber + 1
    outfile.write("R000"+str(totalRecords))

if __name__ == '__main__':
    main(sys.argv[1:])                
